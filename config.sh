# *UEFI boot [0/1]
# (defaults to 1)
UEFI=


# +---------------------+
# | GUIDED PARTITIONING |
# +---------------------+

# *Enable guided partitioning [0/1]
# (defaults to 1)
GUIDED_PARTITIONING=

# *Partition table [dos/gpt]
# (defaults to gpt)
PARTITION_TABLE=

# *Target disk for the installation [/dev/<disk_name>]
# (not required for the advanced partition setup)
DISK=

# *EFI partition size [<amount>M]
# (defaults to 200M)
EFI_SIZE=

# *Root size [<amount>G]
# (defaults to +)
ROOT_SIZE=

# *Root filesystem [ext4/btrfs]
# (defaults to ext4)
ROOT_FS=

# Separate home partition size [<amount>G]
# (won't be created if blank)
HOME_SIZE=

# Home filesystem [ext4/btrfs]
HOME_FS=

# Separate swap partition size [<amount>G]
# (won't be created if blank)
SWAP_SIZE=

# Partition order [swap,root,home]
# (defaults to swap,home,root)
PART_ORDER=

# Disk encryption password
# (disk encryption won't be enabled if left blank)
LUKS_PASSWD=

# LUKS containter size
# (defaults to + if left blank)
LUKS_SIZE=

# +---------+
# | LOCALES |
# +---------+

LANG=
LC_ALL=
KEYMAP=


# +-------+
# | USERS |
# +-------+

ROOT_PWD=
USER=
WHEEL=
USER_PWD=
