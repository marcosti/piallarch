source ./utils/error_colors.sh
source ./config.sh

BYTE_DISK_SIZE=
PARTITION_SIZE_WILDCARD=0

_DISK_PARTITION_TABLE=()

collect_disk_information() {
  _DISK_PARTITION_TABLE+=( "$1" )
}

size_to_bytes() {
  local part_size="$1"
  if echo "$part_size" | grep 'M[[:space:]]*$' &>/dev/null
  then
    echo "$(echo "$part_size" | sed 's/[MG]//g')*(1024^2)" | bc
  elif echo "$part_size" | grep 'G[[:space:]]*$' &>/dev/null
  then
    echo "$(echo "$part_size" | sed 's/[MG]//g')*(1024^3)" | bc
  fi
}

disk_space_check() {
  [[ -z "$DISK" ]] && echo "`ERROR`: cannot check size of undefined DISK" && return 1
  declare -i parts_sum=0
  declare -i BYTE_DISK_SIZE=$(lsblk -b --output SIZE -d $DISK | tail -1)
  local part_max=

  for part in "${_DISK_PARTITION_TABLE[@]}"
  do
    local part_name="$(echo "$part" | cut -d ";" -f 1)"
    local part_size="$(echo "$part" | cut -d ";" -f 2)"
    [[ -z "$part_size" ]] && echo "`ERROR`: invalid partition size \"$part_size\" for \"$part_name\"" && continue
    [[ "$part_size" == "+" ]] && part_size="${part_size//+/#MAX}"

    if [[ "$part_size" == "#MAX" ]]
    then
      if [[ "$part_name" == "EFI_SIZE" ]]
      then
        echo "`ERROR`: please define how big the EFI partition needs to be" && return 1
      fi
      if ! ((PARTITION_SIZE_WILDCARD))
      then
        part_max="$part_name"
        PARTITION_SIZE_WILDCARD=1
        continue
      else
        echo "`ERROR`: the size wildcard (+) was used for more than one partition"
        return 1
      fi
    fi

    part_size="$(size_to_bytes "$part_size")"
    parts_sum=$(echo "$parts_sum+$part_size" | bc)
  done

  ((parts_sum > BYTE_DISK_SIZE)) && echo "`ERROR`: the sum of the defined partitions ($parts_sum) is greater than the disk size ($BYTE_DISK_SIZE)" && return 1

  if ((PARTITION_SIZE_WILDCARD))
  then
    eval "${part_max}=$(echo "$((BYTE_DISK_SIZE-parts_sum))/(1024^2)" | bc)M"
    echo "`INFO`: setting \"$part_max\" to ${!part_max}"
  fi

  return 0
}
