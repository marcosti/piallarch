source ./utils/error_colors.sh
source ./utils/disk_space_check.sh

declare -a VARS=(
  "UEFI;1;0,1"

  "GUIDED_PARTITIONING;1;0,1"
  "PARTITION_TABLE;gpt;dos,gpt"
  "DISK;;#DISK"
  "EFI_SIZE;200M;#SIZE"
  "ROOT_SIZE;#MAX;#SIZE"
  "ROOT_FS;ext4;ext4,btrfs"
  "HOME_SIZE;#NULL;#SIZE"
  "HOME_FS;#NULL;ext4,btrfs"
  "SWAP_SIZE;#NULL;#SIZE"
  "PART_ORDER;swap,root;#PORDER"
  "LUKS_PASSWD;#NULL;"
  "LUKS_SIZE;#MAX;#SIZE"
)

check_value() {
  local v="$1"
  local var="$(echo "$v" | cut -d ';' -f 1)"
  local value="$(echo "$v" | cut -d ';' -f 2)"
  local type="$(echo "$v" | cut -d ';' -f 3)"

  local ret=0

  [[ "$value" == "#NULL" ]] && return 0
  [[ -z "$value" ]] && return 1
  [[ -z "$type" ]] && return 0

  if echo "$type" | grep "^#" &>/dev/null
  then
    case "$type" in
      "#SIZE")
        if [[ "$value" == "+" || "$value" == "#MAX" ]]
        then
          collect_disk_information "$var;$value"
        else
          echo "$value" | grep "^[0-9]*[MmGg]" &>/dev/null || ret=1
          collect_disk_information "$var;$value"
        fi
        ;;
      "#PORDER")
        local home=0
        local root=0
        local swap=0
        for part in $(echo "$value" | sed 's/,/ /g')
        do
          if [[ "$part" == "home" ]]
          then
            if !((home))
            then
              home=1
            else
              echo "`ERROR`: home partition was already defined" && ret=1
            fi
          elif [[ "$part" == "root" ]]
          then
            if !((root))
            then
              root=1
            else
              echo "`ERROR`: root partition was already defined" && ret=1
            fi
          elif [[ "$part" == "swap" ]]
          then
            if !((swap))
            then
              swap=1
            else
              echo "`ERROR`: swap partition was already defined" && ret=1
            fi
          else
            echo "`ERROR`: unknown partition type \"$part\"" && ret=1
          fi
        done
        ;;
      "#DISK")
        [[ ! -b "$value" ]] && echo "`ERROR`: \"$value\" does not appear to be a disk" && ret=1
        ;;
      *)
        echo "`ERROR`: unknown type $type"
        ret=1
        ;;
    esac
  else
    ret=1
    for t in $(echo "$type" | sed 's/,/ /g')
    do
      if [[ "$value" == "$t" ]]
      then
        ret=0
        break
      fi
    done
    ((ret)) && echo "`ERROR`: invalid value \"$value\", acceptable values are [$type]"
  fi

  return $ret
}

check_variables() {
  local ret=0
  for v in ${VARS[@]}
  do
    local var="$(echo "$v" | cut -d ';' -f 1)"
    local type="$(echo "$v" | cut -d ';' -f 3)"
    echo "${!var}" | grep "^[[:space:]]*#" &>/dev/null && echo "`ERROR`: values beginning with \"#\" are reserved values" && ret=1
    if [[ -z "${!var}" ]]; then
      local def="$(echo "$v" | cut -d ';' -f 2)"
      if [[ -n "$def" ]]; then
        echo "`WARNING`: setting $var to $def" 1>&2
        eval "${var}=$def"
      else
        echo "`ERROR`: $var is unset" 1>&2 && ret=1
      fi
    fi
    check_value "$var;${!var};$type" || ret=1
  done

  disk_space_check || ret=1

  return $ret
}
