CLRRESET() {
  echo -n "`tput sgr0`"
}

ERROR() {
  echo -n "`tput setaf 1``tput bold`ERROR`CLRRESET`"
}

WARNING() {
  echo -n "`tput setaf 3``tput bold`WARNING`CLRRESET`"
}

INFO() {
  echo -n "`tput setaf 2``tput bold`INFO`CLRRESET`"
}
