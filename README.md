# piallarch

A work in progress Arch BTW installer which doesn't suck as much as `archinstall`

## Description
These scripts will allow to install Arch BTW Linux with an encrypted root (and home) partition, following the [LVM on LUKS](https://wiki.archlinux.org/title/Dm-crypt/Encrypting_an_entire_system#LVM_on_LUKS) guide on the Arch BTW Wiki

## Usage

```bash
$ ./piallarch
bash: ./piallarch: No such file or directory
```

## Roadmap
- [ ] Install Arch

## License
This project is licenced under GPLv2 and **comes with no warranty**. **Use it as your own risk**. More info about the licence in `LICENCE`
